<?php

namespace ScoRugby\API\Manager;

use ScoRugby\CoreBundle\Manager\ManagerInterface;

/**
 *
 * @author Antoine BOUET
 */
interface APIManagerInterface extends ManagerInterface {
    
}
