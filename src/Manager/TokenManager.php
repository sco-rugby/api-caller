<?php

namespace ScoRugby\API\Manager;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\HttpClient\HttpClientInterface;

/**
 * Description of TokenManager
 *
 * @author Antoine BOUET
 */
class TokenManager implements TokenManagerInterface {

    private ?string $accessToken = null;

    public function __construct(private readonly HttpClientInterface $httpClient, private readonly ResponseHandler $responseHandler, private readonly string $clientId, private readonly string $clientSecret) {
        return;
    }

    public function getAccessToken(): string {
        return $this->accessToken;
    }

    public function refresh(): string {
        return $this->accessToken;
    }
}
