<?php

namespace ScoRugby\API\Manager;

/**
 *
 * @author Antoine BOUET
 */
interface TokenManagerInterface {

    public function getAccessToken(): string;

    public function refresh(): string;
}
