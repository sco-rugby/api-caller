<?php

namespace ScoRugby\API\Manager;

use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;
use ScoRugby\CoreBundle\Manager\ManagerInterface;
use ScoRugby\API\Service\APICallerInterface;
use Doctrine\Common\Collections\Collection;

abstract class AbstractAPIManager extends ManagerInterface {

    private ?string $baseUrl = null;
    protected array $queryParams = [];

    public function __construct(private APICallerInterface $caller, private ContainerBagInterface $params) {
        return;
    }

    protected function getUrl(?string $verb = null): ?string {
        $url = $this->getBaseUrl() . '/' . $this->getResourceName();
        if (null !== $verb) {
            $url .= '/' . $verb;
        }
        return encode_url(strtolower($url));
    }

    public function getBaseUrl(): ?string {
        return $this->baseUrl;
    }

    public function clearRequestParameters(): void {
        $this->queryParams = [];
    }

    public function addRequestParameter(QueryParams $key, mixed $value): void {
        if (is_bool($value) & true === $value) {
            $value = 'true';
        } elseif (is_bool($value) & false === $value) {
            $value = 'false';
        }
        $this->queryParams[$key->value] = $value;
    }

    public function setRequestParameters(array $params): void {
        $this->queryParams = $params;
    }

    public function get($id): ManagedResourceInterface {
        if ($this->isValid()) {
            return $this->caller->get($this->getUrl($id), $this->getClassName());
        }
    }

    public function count(): int {
        if ($this->isValid()) {
            $nb = $this->caller->get($this->getUrl('count'), 'json', ['query' => $this->queryParams]);
            return intval($nb);
        }
    }

    public function create($properties): ManagedResourceInterface {
        if ($this->isValid()) {
            if( !($properties instanceof $this->getClassname())) {
                throw \InvalidArgumentException(sprintf('Parameter MUST be an instance of %s. %s given.', $this->getClassname() . get_class($properties)));
            }
            return $this->caller->post($this->getUrl(), $properties, $this->getClassName());
        }
    }

    public function find(): Collection {
        if ($this->isValid()) {
            return $this->caller->get($this->getUrl(), Collection::class, ['query' => $this->queryParams]);
        }
    }

    public function update($properties): ManagedResourceInterface {
        if ($this->isValid()) {
            if (!($properties instanceof $this->getClassname()) ) {
                throw \InvalidArgumentException(sprintf('Parameter MUST be an instance of %s. %s given.', $this->getClassname() . get_class($properties)));
            }
            return $this->caller->put($this->getUrl(), $properties, $this->getClassName());
        }
    }

    public function delete(): bool {
        if ($this->isValid()) {
            return $this->caller->delete($this->getUrl($this->getResource()->getId()));
        } else {
            return false;
        }
    }

    public function setResource(ManagedResourceInterface $resource): self {
        $this->resource = $resource;
    }

    public function &getResource(): ?ManagedResourceInterface {
        return $this->resource;
    }

    public function isValid(): bool {
        if (empty($this->getBaseUrl())) {
            throw new BadRestUrlException('No base url provided');
        }
        if (empty($this->getResource())) {
            throw new BadRestUrlException('No resource provided');
        }
        if (null != $this->getResource()) {
            $class = $this->getClassName();
            return ($this->getResource() instanceof $class);
        } else {
            return true;
        }
    }

    public function first() {
        if ($this->isValid()) {
            return $this->caller->get($this->getUrl('first'));
        }
    }

    public function last() {
        if ($this->isValid()) {
            return $this->caller->get($this->getUrl('last'));
        }
    }

    public function setPagination(int $start, int $end) {
        $this->addRequestParameter('range', $start . '-' . $end);
    }

    public function getPagination(): array {
        $range = [];
        if (key_exists('range', $this->queryParams)) {
            $range = explode('-', $this->queryParams['range']);
        }
        return $range;
    }

    public function addPartialResponse($field): void {
        $fields = [];
        if (key_exists('fields', $this->queryParams)) {
            $fields = explode(',', $this->queryParams['fields']);
        }
        $fields[] = $field;
        $this->setPartialResponse($fields);
    }

    public function setPartialResponse(array $fields): void {
        $this->addRequestParameter('fields', implode(',', $fields));
    }

    public function clearPartialResponse(): void {
        if (key_exists('fields', $this->queryParams)) {
            unset($this->queryParams['fields']);
        }
    }

    public function addSortFields($field): void {
        $fields = [];
        if (key_exists('sort', $this->queryParams)) {
            $fields = explode(',', $this->queryParams['sort']);
        }
        $fields[] = $field;
        $this->setSortFields($fields);
    }

    public function setSortFields(array $fields): void {
        $this->addRequestParameter('sort', implode(',', $fields));
    }

    public function clearSortFields(): void {
        if (key_exists('sort', $this->queryParams)) {
            unset($this->queryParams['sort']);
        }
    }

    public function addDescFields($field): void {
        $fields = [];
        if (key_exists('desc', $this->queryParams)) {
            $fields = explode(',', $this->queryParams['desc']);
        }
        $fields[] = $field;
        $this->setSortFields($fields);
    }

    public function setDescFields(array $fields): void {
        $this->addRequestParameter('desc', implode(',', $fields));
    }

    public function clearDescFields(array $fields): void {
        if (key_exists('desc', $this->queryParams)) {
            unset($this->queryParams['desc']);
        }
    }
}
