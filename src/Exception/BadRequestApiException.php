<?php

namespace ScoRugby\API\Exception;

/**
 * Description of BadRequestAPIException
 *
 * @author Antoine BOUET
 */
class BadRequestAPIException extends \Exception {
    
}
