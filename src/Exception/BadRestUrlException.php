<?php

namespace ScoRugby\API\Exception;

/**
 * Description of badRestUrlException
 *
 * @author Antoine BOUET
 */
class BadRestUrlException extends APIException {
    
}
