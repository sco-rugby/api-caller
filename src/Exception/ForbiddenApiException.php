<?php

namespace ScoRugby\API\Exception;

/**
 * Description of ForbiddenAPIException
 *
 * @author Antoine BOUET
 */
class ForbiddenAPIException extends APIException {
    
}
