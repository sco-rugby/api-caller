<?php

namespace ScoRugby\API\Service;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use ScoRugby\API\Manager\TokenManagerInterface;
use ScoRugby\API\Handler\ResponseHandler;

/**
 * Description of ApiCaller
 *
 * @author Antoine BOUET
 */
class APICaller implements APICallerInterface {

    public function __construct(private readonly HttpClientInterface $httpClient, private readonly TokenManagerInterface $tokenManager, private readonly ResponseHandler $responseHandler, private readonly SerializerInterface $serializer) {
        return;
    }

    public function get(string $url, string $responseClassType, ?array $options = []): Object {

        $response = $this->httpClient->request(Request::METHOD_GET, $url, array_merge([
            'auth_bearer' => $this->tokenManager->getAccessToken(),
                        ], $options));

        return $this->responseHandler->deserialize($response, $responseClassType);
    }

    public function post(string $url, array|Object|null $body, string $responseClassType, ?array $options = []): Object {
        $response = $this->httpClient->request(Request::METHOD_POST, $url, array_merge([
            'auth_bearer' => $this->tokenManager->getAccessToken(),
            'body' => $this->serializer->serialize($body, 'json'),
                        ], $options));

        return $this->responseHandler->deserialize($response, $responseClassType);
    }

    public function put(string $url, array|Object|null $body, string $responseClassType, ?array $options = []): Object {
        $response = $this->httpClient->request(Request::METHOD_PUT, $url, array_merge([
            'auth_bearer' => $this->tokenManager->getAccessToken(),
            'body' => $this->serializer->serialize($body, 'json'),
                        ], $options));

        return $this->responseHandler->deserialize($response, $responseClassType);
    }

    public function patch(string $url, array|Object|null $body, string $responseClassType, ?array $options = []): Object {
        $response = $this->httpClient->request(Request::METHOD_PATCH, $url, array_merge([
            'auth_bearer' => $this->tokenManager->getAccessToken(),
            'body' => $this->serializer->serialize($body, 'json'),
                        ], $options));

        return $this->responseHandler->deserialize($response, $responseClassType);
    }

    public function delete(string $url, ?array $options = []): ResponseInterface {
        return $this->httpClient->request(Request::METHOD_DELETE, $url, array_merge(['auth_bearer' => $this->tokenManager->getAccessToken()], $options));
    }
}
