<?php

namespace ScoRugby\API\Service;

/**
 *
 * @author Antoine BOUET
 */
interface APICallerInterface {

    public function get(string $url, string $responseClassType, ?array $options = []): Object;

    public function post(string $url, array|Object|null $body, string $responseClassType, ?array $options = []): Object;

    public function patch(string $url, array|Object|null $body, string $responseClassType, ?array $options = []): Object;

    public function delete(string $url, ?array $options = []): ResponseInterface;
}
