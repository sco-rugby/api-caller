<?php

namespace ScoRugby\API\Handler;

use Symfony\Contracts\HttpClient\ResponseInterface;

/**
 *
 * @author Antoine BOUET
 */
interface ResponseHandlerInterface {

    public function deserialize(ResponseInterface $response, string $responseClassType): Object;
}
