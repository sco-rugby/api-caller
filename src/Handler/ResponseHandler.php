<?php

namespace ScoRugby\API\Handler;

use ScoRugby\CoreBundle\Exception\API\APIException;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Contracts\HttpClient\Exception\HttpExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

/**
 * Description of ResponseHandler
 *
 * @author Antoine BOUET
 */
class ResponseHandler implements ResponseHandlerInterface {

    public function __construct(private readonly SerializerInterface $serializer) {
        return;
    }

    public function deserialize(ResponseInterface $response, string $responseClassType): Object {
        try {
            $responseContent = $response->getContent();
        } catch (HttpExceptionInterface | TransportExceptionInterface $e) {
            try {
                $content = $response->getContent(false);
            } catch (HttpExceptionInterface | TransportExceptionInterface $e) {
                $content = 'unknown error';
            }

            throw new APIException($e->getMessage() . ' : ' . $content);
        }

        return $this->serializer->deserialize($responseContent, $responseClassType, 'json');
    }
}
